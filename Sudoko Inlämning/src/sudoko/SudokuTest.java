package sudoko;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SudokuTest 
{
	private SudokuSolver sudoku;
	@BeforeEach
	void setUp() throws Exception 
	{
		sudoku = new Sudoku();
	}

	@AfterEach
	void tearDown() throws Exception 
	{
		sudoku.clear();
	}
	
	@Test
	void testSolvesSudoku() 
	{
		sudoku.add(0, 2, 8);
		sudoku.add(0, 5, 9);
		sudoku.add(0, 7, 6);
		sudoku.add(0, 8, 2);
		sudoku.add(1, 8, 5);
		sudoku.add(2, 0, 1);
		sudoku.add(2, 2, 2);
		sudoku.add(2, 3, 5);
		sudoku.add(3, 3, 2);
		sudoku.add(3, 4, 1);
		sudoku.add(3, 7, 9);
		sudoku.add(4, 1, 5);
		sudoku.add(4, 6, 6);
		sudoku.add(5, 0, 6);
		sudoku.add(5, 7, 2);
		sudoku.add(5, 8, 8);
		sudoku.add(6, 0, 4);
		sudoku.add(6, 1, 1);
		sudoku.add(6, 3, 6);
		sudoku.add(6, 5, 8);
		sudoku.add(7, 0, 8);
		sudoku.add(7, 1, 6);
		sudoku.add(7, 4, 3);
		sudoku.add(7, 6, 1);
		sudoku.add(8, 6, 4);
		
		sudoku.solve(0,0);
		assertTrue(sudoku.get(0, 0) == 5);
		assertTrue(sudoku.get(0, 1) == 4);
		assertTrue(sudoku.get(0, 3) == 1);
		assertTrue(sudoku.get(0, 4) == 7);
		assertTrue(sudoku.get(0, 6) == 3);
		assertTrue(sudoku.isValid());
	}
	
	@Test
	void testEmptySudoku()
	{
		sudoku.solve(0,0);
		assertTrue(sudoku.isValid());
	}
	
	@Test
	void testNonSolutionSudoku()
	{
		sudoku.add(0, 2, 8);
		sudoku.add(0, 5, 9);
		sudoku.add(0, 7, 6);
		sudoku.add(0, 8, 2);
		sudoku.add(1, 8, 5);
		sudoku.add(2, 0, 1);
		sudoku.add(2, 2, 2);
		sudoku.add(2, 3, 5);
		sudoku.add(3, 3, 2);
		sudoku.add(3, 4, 1);
		sudoku.add(3, 7, 9);
		sudoku.add(4, 1, 5);
		sudoku.add(4, 6, 6);
		sudoku.add(5, 0, 6);
		sudoku.add(5, 7, 2);
		sudoku.add(5, 8, 8);
		sudoku.add(6, 0, 4);
		sudoku.add(6, 1, 1);
		sudoku.add(6, 3, 6);
		sudoku.add(6, 5, 8);
		sudoku.add(7, 0, 8);
		sudoku.add(7, 1, 6);
		sudoku.add(7, 4, 3);
		sudoku.add(7, 6, 1);
		sudoku.add(8, 6, 4);
		
		sudoku.add(8, 2, 5);
		sudoku.solve(0,0);
		assertFalse(sudoku.isValid());
	}
	@Test
	void testClear()
	{
		sudoku.add(1, 3, 8);
		sudoku.add(3, 1, 1);
		sudoku.add(3, 3, 2);
		sudoku.clear();
		assertTrue(sudoku.get(1, 3) == 0);
		assertTrue(sudoku.get(3, 1) == 0);
		assertTrue(sudoku.get(3, 3) == 0);		
	}
	
	@Test
	void testRemove()
	{
		sudoku.add(0, 2, 8);
		sudoku.add(2, 0, 1);
		sudoku.add(2, 2, 2);
		sudoku.remove(2, 2);
		assertTrue(sudoku.get(0, 2) == 8);
		assertTrue(sudoku.get(2, 2) == 0);
	}
	
	@Test
	void testGet()
	{
		sudoku.add(8, 8, 9);
		sudoku.add(3, 4, 3);
		assertTrue(sudoku.get(8, 8) == 9);
		assertTrue(sudoku.get(3, 4) == 3);
	}
	
	@Test
	void testGetIllegalArgument() throws IllegalArgumentException
	{ //Denna ska ge IllegalArgumentError (ska bli rött).
		sudoku.add(3, 5, 12);
	}
	
	@Test
	void testAdd()
	{
		sudoku.add(0, 2, 8);
		sudoku.add(2, 0, 1);
		sudoku.add(2, 0, 5);
		sudoku.add(2, 0, 4);
		assertTrue(sudoku.get(0, 2) == 8);
		assertTrue(sudoku.get(2, 0) == 4);
	}
	@Test
	void testAddIlleagalArgument() throws IllegalArgumentException
	{ //Denna ska ge IllegalArgumentError (ska bli rött).
		sudoku.add(10, 10, 1);
	}
	
	@Test
	void testSetMatrix()
	{
		int[][] m = new int[9][9];
		m[0][0] = 4;
		m[0][1] = 1;
		sudoku.setMatrix(m);
		assertTrue(sudoku.get(0, 0) == 4);
		assertTrue(sudoku.get(0, 1) == 1);
	}
	
	@Test
	void testGetMatrix()
	{
		sudoku.add(0, 0, 5);
		assertTrue(sudoku.getMatrix()[0][0] == 5);
	}

}
