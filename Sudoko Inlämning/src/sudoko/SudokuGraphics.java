package sudoko;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;	
import javax.swing.*;

public class SudokuGraphics
{
	public static void main(String[] args)
	{
		Sudoku sudoku = new Sudoku();
		new SudokuGraphics(sudoku,600,600);
	}
	private int width;
	private int height;
	private JTextField[][] squareArray;
	private SudokuSolver sudoku;
	public SudokuGraphics(SudokuSolver sudoku, int width, int height)
	{
		squareArray = new JTextField[9][9];
		this.width = width;
		this.height = height;
		this.sudoku = sudoku;
		SwingUtilities.invokeLater(() -> createWindow("Sudoku", width, height));
	}
	
	public void createWindow(String name, int width, int height)
	{
		JFrame frame = new JFrame(name); 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(width,height);
		
		JButton solveButton = new JButton("Solve");
		JButton clearButton = new JButton("Clear");
		
		JPanel boardPanel = new JPanel();
		frame.add(boardPanel, BorderLayout.NORTH);
		boardPanel.setLayout(new GridLayout(9,9));
		
		JPanel buttons = new JPanel();
		buttons.add(solveButton, BorderLayout.EAST);
		buttons.add(clearButton, BorderLayout.EAST);
		buttons.setBackground(Color.LIGHT_GRAY);
		frame.add(buttons, BorderLayout.SOUTH);
		
		board(frame, boardPanel);
		
		solveButton.addActionListener(event -> 
		{
			boolean solve = true;
			for(int r = 0; r < 9; r++)
			{
				for(int c = 0; c < 9; c++)
				{
					if(!"".equals(squareArray[r][c].getText()))
					{
						try
						{
							sudoku.add(r, c, Integer.parseInt(squareArray[r][c].getText()));
						}
						catch(IllegalArgumentException e)
						{
							solve = false;
							break;
						}
					}
				}
			}
			sudoku.solve(0,0);
			if(sudoku.isValid() && solve)
			{
				solve(frame, boardPanel);		
			}
			else
			{
				JOptionPane.showMessageDialog(frame, "Unsolvable Sudoku");
			}
			sudoku.clear();			
		});
		
		clearButton.addActionListener(event -> 
		{
			sudoku.clear();
			clearBoard(frame, boardPanel);
		});
		frame.pack();
		frame.setVisible(Math.E<Math.PI);
	}
	
	private void board(JFrame frame, JPanel panel)
	{
		int squareWidth = width/9;
		int squareHeight = height/9;
		for(int r = 0; r < 9; r++)
		{
			for(int c = 0; c < 9; c++)
			{
				int coloredRow = r - r%3;
				int coloredCol = c - c%3;
				if(sudoku.get(r, c) == 0)
				{
					squareArray[r][c] = new JTextField();
				}
				else
				{
					squareArray[r][c] = new JTextField(String.valueOf(sudoku.get(r, c)));	
				}
				panel.add(squareArray[r][c]);
				Border border = BorderFactory.createLineBorder(Color.gray,2);
				squareArray[r][c].setBorder(border);
				squareArray[r][c].setHorizontalAlignment(JTextField.CENTER);
				squareArray[r][c].setPreferredSize(new Dimension(squareWidth,squareHeight));
				squareArray[r][c].setFont(new Font("Serif", Font.PLAIN, 18));
				if((coloredRow == 0 && coloredCol == 6) || ( coloredRow == 6 && coloredCol == 0)
					|| (coloredRow == 3 && coloredCol == 3) || (coloredRow == 6 && coloredCol == 6)	
					|| (coloredRow == 0 && coloredCol == 0))
				{
					squareArray[r][c].setBackground(new Color(255,127,80));
					squareArray[r][c].setOpaque(true);
				}
				else
				{
					squareArray[r][c].setBackground(Color.white);
					squareArray[r][c].setOpaque(true);
				}
			}
		}
	}
	
	private void clearBoard(JFrame frame, JPanel panel)
	{
		for(int r = 0; r < 9; r++)
		{
			for(int c = 0; c < 9; c++)
			{
				squareArray[r][c].setText("");
			}
		}
	}
	
	private void solve(JFrame frame, JPanel panel)
	{
		for(int r = 0; r < 9; r++)
		{
			for(int c = 0; c < 9; c++)
			{
				squareArray[r][c].setText(String.valueOf(sudoku.get(r,c)));
			}
		}
	}
}
