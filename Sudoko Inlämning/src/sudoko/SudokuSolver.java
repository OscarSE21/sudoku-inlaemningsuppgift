package sudoko;

public interface SudokuSolver 
{
	/**
	 * Solves the sudoku matrix starting from the square (r,c). Returns true if it is
	 * possible to solve the sudoku at the square [r,c].
	 * 
	 * @param r   starts solving at row r.
	 * @param c   starts solving at col c. 
	 */
	boolean solve(int r, int c);

	/**
	 * Puts digit in the box row, col.
	 * 
	 * @param row   The row
	 * @param col   The column
	 * @param digit The digit to insert in box row, col
	 * @throws IllegalArgumentException if row, col or digit is outside the range
	 *                                  [0..9]
	 */
	void add(int row, int col, int digit);

	/**
	 * Removes the digit at the position [row][col] from the sudoku board.
	 * 
	 *  
	 * @param row   removes digit at row: row.
	 * @param col    removes digit at col: col.
	 * @param digit  removes the digit: digit. 
	 * @throws IllegalArgumentException if row or col is out of bounds: not in [0,8].
	 */
	void remove(int row, int col);

	/**
	 * Returns the digit at the position [row][col] from the sudoku board. 
	 * 
	 * @param row   row: row in the sudoku board.
	 * @param column: col in the sudoku board. 
	 * @throws IllegalArgumentException if row or col is out of bounds: not in [0,8].
	 */
	int get(int row, int col);

	/**
	 * Checks that all filled in digits follows the the sudoku rules, if so, returns true.
	 * If not, returns false.
	 */
	boolean isValid();

	/**
	 * Clears the sudoku board. All digits are filled in with 0. 
	 */
	void clear();

	/**
	 * Fills the grid with the digits in m. The digit 0 represents an empty box.
	 * 
	 * @param m the matrix with the digits to insert
	 * @throws IllegalArgumentException if m has the wrong dimension or contains
	 *                                  values outside the range [0..9]
	 */
	void setMatrix(int[][] m);

	/**
	 * Returns the sudoku board matrix. 
	 */
	int[][] getMatrix();
}
