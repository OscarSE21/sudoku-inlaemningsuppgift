package sudoko;

public class Sudoku implements SudokuSolver
{
	private int[][] board;
	public Sudoku() 
	{
		board = new int[9][9];
		emptyBoard();
	}

	private void emptyBoard()
	{
		for(int r = 0; r < board[0].length; r++)
		{
			for(int c = 0; c < board.length; c++)
			{
				board[r][c] = 0;
			}
		}
	}
	
	public boolean solves()
	{
		return solves(0,0);
	}
	
	private boolean solves(int r, int c)
	{
		return solve(r,c);
	}
	
	public boolean solve(int r, int c) 
	{	
		if(r == 9)
		{
			return true;
		}
		if(board[r][c] == 0)
		{
			for(int digit = 1; digit <= 9; digit++)
			{
				if(solvable(r,c,digit))
				{
					board[r][c] = digit;
					if(solve(r,c))
					{
						return true;
					}
					else
					{
						board[r][c] = 0;
					}
				}
			}
			return false;
		}
		else
		{
			if(c != 8)
			{
					c++;
			}
			else
			{
				c = 0;
				r++;
			}
			return solve(r,c);	
		}
	}
	
	public boolean solvable(int r, int c, int digit)
	{
		for(int i = 0; i < 9; i++)
		{
			if(board[r][i] == digit && c != i)
			{
				return false;
			}
		}
		
		for(int i = 0; i < 9; i++)
		{
			if(board[i][c] == digit && r != i)
			{
				return false;
			}
		}
		
		int startBoxRow = r - r%3;
		int startBoxCol = c - c%3;
		
		for(int r0 = startBoxRow; r0 < startBoxRow+3;r0++)
		{
			for(int c0 = startBoxCol; c0 < startBoxCol+3;c0++)
			{
				if(board[r0][c0] == digit)
				{
					return false;
				}
			}
		}
		
		if(digit > 9 || digit < 0)
		{
			return false;
		}
		return true;
	}

	@Override
	public void add(int row, int col, int digit) 
	{
		if(!solvable(row,col,digit) && board[row][col] == 0)
		{
			throw new IllegalArgumentException("Sudokut ej lösbart");
		}
		if(row > 8 || row < 0 || col > 8 || col < 0 || digit > 9 || digit < 0)
		{
			throw new IllegalArgumentException("Ej valbar kolumn eller rad");
		}
		else
		{
			board[row][col] = digit;
		}
	}

	@Override
	public void remove(int row, int col) 
	{
		if(row > 8 || row < 0 || col > 8 || col < 0)
		{
			throw new IllegalArgumentException();
		}
		if(board[row][col] != 0)
		{
			board[row][col] = 0;
		}
	}

	@Override
	public int get(int row, int col) 
	{
		if(row > 8 || row < 0 || col > 8 || col < 0)
		{
			throw new IllegalArgumentException();
		}
		else
		{
			return board[row][col];	
		}
	}

	@Override
	public boolean isValid() 
	{
		for(int r= 0; r < 9; r++)
		{
			for(int c = 0; c < 9; c++)
			{
				int checkCol = 0;
				int number = board[r][c];
				while(checkCol < 9)
				{
					if(board[r][checkCol] == number  && checkCol != c)
					{
						return false;
					}
					checkCol++;
				}
			}
		}
		
		for(int r= 0; r < 9; r++)
		{
			for(int c = 0; c < 9; c++)
			{
				int checkRow = 0;
				int number = board[r][c];
				while(checkRow < 9)
				{
					if(board[checkRow][c] == number && checkRow != r)
					{
						return false;
					}
					checkRow++;
				}
			}
		}
		
		for(int r = 0; r < 9; r++)
		{
			for(int c = 0; c < 9; c++)
			{
				int number = board[r][c];
				int startBoxRow = r - r%3;
				int startBoxCol = c - c%3;
				for(int r0 = startBoxRow; r0 < startBoxRow+3;r0++)
				{
					for(int c0 = startBoxCol; c0 < startBoxCol+3;c0++)
					{
						if(board[r0][c0] == number && (r0 != r && c0 != c))
						{
							return false;
						}
					}
				}
			}
		}
		
		for(int r = 0; r < 9; r++)
		{
			for(int c = 0; c < 9; c++)
			{
				if(board[r][c] == 0)
				{
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void clear() 
	{
		emptyBoard();
	}

	@Override
	public void setMatrix(int[][] m) 
	{
		if(m[0].length > 9 || m.length > 9)
		{
			throw new IllegalArgumentException();
		}
		for(int r = 0; r < 9; r++)
		{
			for(int c = 0; c < 9; c++)
			{
				if(m[r][c] > 9 || m[r][c] < 0)
				{
					throw new IllegalArgumentException();
				}
				else
				{
					board[r][c] = m[r][c];
				}
			}
		}
	}

	@Override
	public int[][] getMatrix() 
	{
		return board;
	}
}
